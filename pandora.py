from bs4 import BeautifulSoup
import urllib2, cookielib

class pandoraRequests():
    user = ""
    
    headers = [('X-Requested-With', 'XMLHttpRequest'),
               ('User-Agent', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)'),
               ('Accept', '*/*'),
               ('Referer', 'http://www.pandora.com/profile/likes/'+user),
               ('Cookie','at=wca8xniGVmjUijKIHDfJQXPXK+OFmuiaA')
               ]
    
    cookie_jar = cookielib.CookieJar()
    urlopener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookie_jar))
    urlopener.addheaders = headers
    
    def __init__(self, user):
        self.user = user
    
    def cond(self,x):
        if x:
            return not "like_context_stationname" in x
        else:
            return True
        
    def getTracks(self,startIndex):
        URL = "http://www.pandora.com/content/tracklikes?likeStartIndex=0&thumbStartIndex="+str(startIndex)+"&webname="+self.user
        response = self.urlopener.open(URL) 
        soup = BeautifulSoup(response.read())
        results = []
        for a in soup.findAll('a',{ "class" : self.cond }):
            results.append(''.join(a.findAll(text=True)))
            
        results = zip(*[results[i::2] for i in range(2)]) 
        
        return results












