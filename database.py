import os
import sqlite3
import string
import sys

class databaseManager():

    db = sqlite3.connect("songs.db")
    
    def __init__(self,directories):
        if directories is not None:
            self.create()
            print 'Beginning Directory Scan...'
            numOfSongs = 0
            for path in directories:
                listings = os.listdir(path)
                for item in listings:
                    name, ext = os.path.splitext(item)
                    numOfSongs += 1
                    if ext == '.mp3':
                        self.put(name,ext)
                        
            print str(numOfSongs) + " songs scanned and managed."
                    
    def check(self, song):
        c = self.db.cursor()
        query = 'SELECT * FROM entry WHERE artist == :artist and title == :title '
        entry = {'artist' : song[1],
                 'title'  : song[0] }
        
        rows = c.execute(query,entry)
        
        for row in rows:
            return True
        
        return False
        
    def create(self):
        try:
            self.db.execute('DROP TABLE entry;')
        except:
            print "No Table (First run or songs.db is missing)"
        self.db.execute('CREATE TABLE entry (title varchar(255), artist varchar(255), filename varchar(255));')
    
    def put(self,song, ext):
        details = [x.strip() for x in string.split(song,"by")]
        entryDict = {
            'title'    : details[0],
            'artist'   : details[1],
            'filename' : song+ext
                     }
    
        query = 'INSERT INTO entry (title, artist, filename) ' + 'VALUES '
        query += '( :title , :artist , :filename );'
    
        self.db.execute(query, entryDict)
        self.db.commit()
    
    def add(self,song):
        entryDict = {
            'title'    : song[0],
            'artist'   : song[1],
            'filename' : song[0] + " by " + song[1] + ".mp3"
                     }
    
        query = 'INSERT INTO entry (title, artist, filename) ' + 'VALUES '
        query += '( :title , :artist , :filename );'
        
        self.db.execute(query, entryDict)
        self.db.commit()
        
    
    def closeDB(self):
        self.db.close()