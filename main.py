import Youtube
import time
import sys
from database import databaseManager
from pandora import pandoraRequests


def sync(song):
        print song
        entry = Youtube.getFirstResult(song)
        entry = Youtube.Check(entry, song)
        
        if entry is not None:       
            url = Youtube.getSafeURL(entry)
            Youtube.Convert(url, song)
        else:
            print "Unable to locate this song, skipping"
            print "------------------------------------"


if __name__ == '__main__':
    repeat = True
    interval = 10 #In seconds, how long to wait to check for new songs
    Firstrun = False
    
    if Firstrun == True:
        dbmanager = databaseManager(('../Music_Output/',))
    else:
        dbmanager = databaseManager(None)
     
    requester = pandoraRequests('zakjustinpandora')
    
    while True:
        noChanges = 0
        for song in requester.getTracks(0):
            track = song[0]+" by "+song[1]
            if dbmanager.check(song) == False:
                sync(track)
                dbmanager.add(song)
            else:
                noChanges+=1
                if noChanges == 5:
                    print "No new songs. ",
       
        if repeat == False:
            break
        print '--- ',
        for i in range(interval):
            time.sleep(1)
            sys.stdout.write("Waiting: " + str(10-i) + " seconds.")
            sys.stdout.flush()
            
