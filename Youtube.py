import gdata.youtube.service
import os
import re
import youtube_dl

# Get the YouTube Service for this session and set the DEV API key and Client ID we're going to use
yt_service = gdata.youtube.service.YouTubeService()
yt_service.developer_key = 'AI39si7Nbhug3f-eYXX9QBFtzE4YbH6T-boNZvxVDPkEpNuKOJul85whWkJoH3xU6X1Ry2CNeZmNFjoJ4eZZbd3fT8PhAPEM7Q'
yt_service.client_id = 'My-Client_id'

# Turn on HTTPS/SSL access.
# Note: SSL is not available at this time for uploads.
yt_service.ssl = False


# Top Level Function for Searching for videos
def SearchAndPrint(search_terms):
    print "Attempting to search for %s" %search_terms
    query = gdata.youtube.service.YouTubeVideoQuery()
    query.vq = search_terms
    feed = yt_service.YouTubeQuery(query)
    PrintVideoFeed(feed)

# Function to get and print a query for videos
def PrintVideoFeed(feed):
    print '------------------------'
    for entry in feed.entry:
        PrintEntryDetails(entry)


#simply return the first unformated result
def getFirstResult(search_terms):
    print "Attempting to search for %s" %search_terms
    query = gdata.youtube.service.YouTubeVideoQuery()
    query.vq = search_terms
    feed = yt_service.YouTubeQuery(query)
    for entry in feed.entry:
            return entry

#Default print function for videos
def PrintEntryDetails(entry):
    print 'Video title: %s' % entry.media.title.text
    print 'Video URL: %s' % entry.media.player.url
    print 'Video duration: %s' % entry.media.duration.seconds
    print '------------------------'


def getSafeURL(entry):
    return re.sub('&feature=youtube_gdata_player','',entry.media.player.url)

#Check the video to see if it matches, return the same object if it does, or the new object if it does not
def Check(entry, title):
    if entry is None:
        print 'Nothing. Trying again.'
        entry = getFirstResult(title)  
        if entry is None:
            print 'No video found'
            return entry
        
    have = re.findall("\w+", (entry.media.title.text).lower())
    want = re.findall("\w+", (title).lower())
    
    perfect = float(len(want))
    score = float(0.0)
    
    for check in map(lambda word: word in have,want):
        if check:
            score+=1.0
    
    score = float(score/perfect)
    
    print 'Found a video, confidence score: %s' % score
    
    if score < .3:
        return None
      
    return entry

#Convert the URL to MP3
def Convert(uri, title):
    youtube_dl.main(uri, title)
    os.system('convert\\bin\\ffmpeg -loglevel quiet -i ' + '"../Music_Output/'+ title + '.mp4" -q 1 ' + ' "../Music_Output/'+ title + '.mp3"')
    os.system('del "../Music_Output/' + title + '.mp4"')
    #print os.system("convert\\bin\\youtube-dl --output ../Music_Output(Test)/%(stitle)s.mp4 --rate-limit 50.0m --extract-audio --audio-format mp3 --audio-quality 0 " + uri)
    print '------------------------'